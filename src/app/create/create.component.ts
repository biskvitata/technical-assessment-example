import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { AppState } from './../app.state';
import { Example } from './../models/example.model'
import * as ExampleActions from './../actions/example.actions';

import { FormGroup, FormControl, Validators } from '@angular/forms'

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  form = new FormGroup({
    date: new FormControl("", [
      Validators.required,
      Validators.minLength(10),
      Validators.maxLength(10)
    ]),
  });

  get date() {
    return this.form.get('date');
  }

  constructor(private store: Store<AppState>) { }

  addDate() {
    this.store.dispatch(new ExampleActions.AddExample({date: this.date.value}));
  }

  ngOnInit() {
  }

}
