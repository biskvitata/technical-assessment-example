import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { Example } from './../models/example.model';
import { AppState } from './../app.state';
import * as ExampleActions from './../actions/example.actions';

@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.css']
})
export class ReadComponent implements OnInit {

  examples: Observable<Example[]>;

  constructor(private store: Store<AppState>) {
    const JSON = require('circular-json');

    this.examples = store.select('example');
    
    this.store.select(state => state).subscribe(example => {
      console.log(JSON.stringify((example)))
    });
  }

  ngOnInit() {
  }

}
