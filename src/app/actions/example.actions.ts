import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'
import { Example } from './../models/example.model'

export const ADD_EXAMPLE = 'ADD_EXAMPLE'

export class AddExample implements Action {
    readonly type = ADD_EXAMPLE

    constructor(public payload: Example) {}
}

export type Actions = AddExample