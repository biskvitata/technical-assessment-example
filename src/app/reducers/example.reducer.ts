import { Action, createSelector } from '@ngrx/store'
import { Example } from './../models/example.model'
import * as ExampleActions from './../actions/example.actions'

let dataId = 1;

export function reducer(state: Example[] = [], action: ExampleActions.Actions) {
    switch(action.type) {
        case ExampleActions.ADD_EXAMPLE:
            return state.concat([Object.assign({}, { id: dataId++ }, action.payload )]);
        
        default:
            return state;
    }
}